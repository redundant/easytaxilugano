FROM node:12

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./package.json
COPY src ./src
COPY public ./public
COPY build ./build

RUN npm install
RUN npm i -g http-server
RUN ls -lah

CMD [ "npm", "run", "serve-heroku" ]