import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import styles from "./HomePage.css";
import axios from "axios";

// require('./global.css');
import "./global.css";

import Slider from "react-slick";
import cityMap from "./cityMap";
// const cityMap = require("./cityMap.json");

let taxiData = {
  languages: {
    it: {
      id: 0,
      ranking: 0,
      page_title: "Taxi",
      page_description:
          "Taxi a __placeholder__? Scegli il nostro Servizio Taxi per viaggiare in sicurezza a __placeholder__ e dintorni, 27/7/365! " +
          "Ti assicuriamo un trasporto efficiente e veloce qualunque sia la tua destinatione, " +
          "nelle maggiori città svizzere, in Italia e in Europa. I nostri servizi includono transfer verso ogni aeroporto e città. ",
      openings: "\"openingHoursSpecification\": [\n" +
          "  {\n" +
          "    \"@type\": \"OpeningHoursSpecification\",\n" +
          "    \"dayOfWeek\": [\n" +
          "      \"Monday\",\n" +
          "      \"Tuesday\",\n" +
          "      \"Wednesday\",\n" +
          "      \"Thursday\",\n" +
          "      \"Friday\",\n" +
          "      \"Saturday\",\n" +
          "      \"Sunday\"\n" +
          "    ],\n" +
          "    \"opens\": \"00:00\",\n" +
          "    \"closes\": \"23:59\"\n" +
          "  }\n" +
          "]",
      keys: {
        places: [
          {name: "Lugano"},
          {name: "Massagno"},
          {name: "Savosa"},
          {name: "Paradiso"},
        ],
        other: [
          {name: "Taxi"},
          {name: "Veloce"},
          {name: "Aereoporti"},
          {name: "Trasporto"},
          {name: "Noleggio"},
          {name: "Conducente"},
        ]
      },
      owner_name: "Emanuele Cardaci",
      business_name: "Easy Taxi Lugano",
      slogans: [
        {text: "Taxi a __placeholder__ e dintorni 24/7. Chiamaci adesso!"},
        {text: "Servizio Taxi a __placeholder__"}
      ],
      email: {
        text: {
          contactUs: "Scrivici per ulteriori informazioni"
        },
        emails: [
          "info@easytaxilugano.ch"
        ]
      },
      telephones: [
        "+41 79 874 60 48"
      ],
      carousel: [
        {imageUrl: "https://s3.amazonaws.com/fasttaxi/t-0/taxi-cover-1.jpg", alt:"A taxi in front of an Airport"},
        {imageUrl: "https://s3.amazonaws.com/fasttaxi/t-0/taxi-cover-2.jpg", alt:"A taxi in front of an Airport"},
        {imageUrl: "https://s3.amazonaws.com/fasttaxi/t-0/taxi-cover-3.jpg", alt:"A taxi in front of an Airport"}
      ],
      cars: {
        text: {
          main: "Siete in tanti? Nessun problema: " +
              "Disponiamo di mezzi moderni che vanno dalla berlina alla station wagon ai van 9 posti."
        },
        cars: [
          {
            model: "Mercedes Class E",
            imageUrl: "https://s3.amazonaws.com/fasttaxi/t-0/mercedes-class-e.jpg",
            alt: "A Mercedes Class E Taxi",
            description: ""
          },
          {
            model: "Mercedes Class E SW",
            imageUrl: "https://s3.amazonaws.com/fasttaxi/t-0/mercedes-benz-class-e-sw.jpg",
            alt: "A Mercedes Class E SW Taxi",
            description: ""
          }
        ]
      },
      trips: {
        text: {from: "Da ", to: "a ", title: "Percorsi Frequenti"},
        routes: [
          {
            from: "Lugano", to: "Milano-Malpensa", price: "200", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Milano-Linate", price: "230", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Orio al Serio", price: "270", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Agno Aereoporto", price: "35/40", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Milano Città", price: "250", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Varese", price: "120", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Como", price: "120", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Bellinzona", price: "100", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Ascona/Locarno", price: "150", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Mendrisio", price: "80", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Chiasso", price: "100", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Ponte Tresa", price: "60", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Rivera (Splash&Spa)", price: "70", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Zurigo", price: "640", unit: "CHF", description: ""
          },
          {
            from: "Lugano", to: "Caslano", price: "50", unit: "CHF", description: ""
          }
        ]
      },
      services: {
        text: {
          main: "Siamo l'azienda leader nel trasporto luganese. " +
              "Taxi Lugano è una realtà che ha saputo fin da subito distinguersi in questo settore, " +
              "diventando un punto di riferimento per i cittadini ticinesi nel mondo della mobilità urbana e del servizio Taxi. \n" +
              "Forniamo servizi di qualità sull’intera area di Lugano e del Canton Ticino e ci rivolgiamo a privati, " +
              "aziende e liberi professionisti"
        },
        services: [
          {
            title: "Trasporto Persone",
            imageUrl: "https://s3.amazonaws.com/fasttaxi/t-0/taxi-cover-1.jpg",
            alt: "Icon displaying a service",
            description: ""
          },
          {
            title: "Transfer Aereoporti",
            imageUrl: "https://s3.amazonaws.com/fasttaxi/t-0/taxi-cover-2.jpg",
            alt: "Icon displaying a service",
            description: ""
          },
          {
            title: "Documenti Riservati",
            imageUrl: "https://s3.amazonaws.com/fasttaxi/t-0/taxi-cover-3.jpg",
            alt: "Icon displaying a service",
            description: ""
          }
        ],
      },
      google: {
        website: "https://www.taxiluganoaeroporti.ch",
        latitude: "46.0027511",
        longitude: "8.9486156",
        address: {
          streetAddress: "Riva Vincenzo Vela",
          addressLocality: "Lugano",
          addressRegion: "CH",
          postalCode: "69000",
          addressCountry: "CH"
        },
        image: "https://s3.amazonaws.com/fasttaxi/t-0/taxi-cover-1.jpg"
      },
      iframe: {
        url: "https://www.taxiluganoaeroporti.ch"
      }
    }
  }
};

let location = "?";
let position = null;
let latitude = null;
let longitude = null;
function getLocation() {
  return new Promise((res, rej) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        position = position;
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        res("Latitude: " + position.coords.latitude +
          ", Longitude: " + position.coords.longitude);
      }, (error) => {
        res(error.code.toString());
      });
    } else {
      res("Geolocation is not supported by this browser.");
    }
  });
}


export class HomePage extends React.Component {
  constructor(props, state) {
    super(props);
    this.state = {
      taxi: taxiData.languages.it
    }
  }

  componentDidMount() {
    let thisRef = this;
    console.log(`${thisRef.constructor.name} componentDidMount`, {styles, props: thisRef.props});
    getLocation().then((result) => {
      console.log(`${thisRef.constructor.name} componentDidMount - getLocation`, {result});
      location = result;
    });
  }

  getStructuredDataScript(taxi) {
    return <script type="application/ld+json">
      {
        `
        {
           "@context":"http://schema.org",
           "@type":"${`LocalBusiness`}",
           "image":[
              "${taxi.google.image}"
           ],
           "@id":"http://fasttaxi.ch/${taxi.id}",
           "name":"${taxi.business_name}",
           "address":{
              "@type":"PostalAddress",
              "streetAddress":"${taxi.google.address.streetAddress}",
              "addressLocality":"${taxi.google.address.addressLocality}",
              "addressRegion":"${taxi.google.address.addressRegion}",
              "postalCode":"${taxi.google.address.postalCode}",
              "addressCountry":"${taxi.google.address.addressCountry}"
           },
           "geo":{
              "@type":"GeoCoordinates",
              "latitude":${taxi.google.latitude},
              "longitude":${taxi.google.longitude}
           },
           "url":"${taxi.google.website}",
           "telephone":"+12122459600",
           ${taxi.openings},
           "priceRange": "$$"
        }
        `
      }
    </script>
  }

  getMainCarousel(taxi) {
    var settings = {
      dots: false,
      autoplay: false,
      autoplaySpeed: 2500,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    const getImage = (item, index) => {
      return (
        <div
          key={index}
        >
          <img src={`${item.imageUrl}`} alt={`${item.alt}`} style={{
            objectFit: "cover",
            width: "100%",
            height: "100%"
          }}/>
        </div>
      );
    };
    return (
      <Slider {...settings}>
        {taxi.carousel.map((carouselItem, index) => {
          return getImage(carouselItem, index)
        })}
      </Slider>
    );
  }

  getCarsCarousel(taxi) {
    const settings = {
      dots: false,
      autoplay: true,
      autoplaySpeed: 1500,
      infinite: true,
      speed: 200,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    const getImage = (carItem, index) => {
      return (
        <div
          key={index}
        >
          <h5>{carItem.model}</h5>
          <p>{carItem.description}</p>
          <div
            className={`${styles["car-item"]}`}
            // style={{backgroundImage: `url(${url})`}}
          >
            <img src={`${carItem.imageUrl}`} alt={`${carItem.alt}`} style={{
              objectFit: "contain",
              width: "100%",
              height: "100%"
            }}/>
          </div>
        </div>
      );
    };
    return (
      <Slider {...settings}>
        {taxi.cars.cars.map((carItem, index) => {
          return getImage(carItem, index)
        })}
      </Slider>
    );
  }

  getTrips(taxi) {
    const getTrip = (routeItem, index, text) => {
      return (
        <div
          key={index}
        >
          <div>{text.from} <b>{routeItem.from}</b> {text.to} <b>{routeItem.to}</b> | <b>{routeItem.price}{routeItem.unit}</b></div>
        </div>
      );
    };
    return (
      <div>
        <h3>{taxi.trips.text.title}</h3>
        {taxi.trips.routes.map((routeItem, index) => {
          return getTrip(routeItem, index, taxi.trips.text)
        })}
      </div>
    );
  }

  getServices(taxi) {
    const settings = {
      dots: false,
      autoplay: true,
      autoplaySpeed: 1500,
      infinite: true,
      speed: 200,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    const getService = (serviceItem, index) => {
      return (
        <div
          key={index}
        >
          <div
            className={`${styles["carousel-item"]}`}
            // style={{backgroundImage: `url(${url})`}}
          >
            <img src={`${serviceItem.imageUrl}`} alt={`${serviceItem.alt}`} style={{
              objectFit: "cover",
              width: "100%",
              height: "100%"
            }}/>
          </div>
        </div>
      );
    };
    return (
      <div>
        <Slider {...settings}>
          {
            taxi.services.services.map((serviceItem, index) => {
              return getService(serviceItem, index);
            })
          }
        </Slider>
      </div>
    );
  }

  capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    let thisRef = this;
    console.log(`${thisRef.constructor.name} render`, {thisRef});
    let taxi = thisRef.state.taxi;
    const getOriginalCity = () => {
      return "Lugano";
    };
    let regexPlaceholder = /__placeholder__/gi;
    return (
      <div className={`${styles.HomePage}`}>
        <Helmet>
          <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.6.0/slick/slick.css"/>
          <link rel="stylesheet" type="text/css"
                href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.6.0/slick/slick-theme.css"/>
          <title>
            Taxi Service Lugano
          </title>
          <meta
            name="description"
            content={taxi.page_description.replace(regexPlaceholder, getOriginalCity()) + `taxi service lugano`}
          />
          {thisRef.getStructuredDataScript(taxi)}
        </Helmet>
        <div className={`section ${styles.section}`}>
          <div className={`carousel`}>
            {thisRef.getMainCarousel(taxi)}
          </div>
        </div>
        <div className={`section ${styles.section}`}>
          <div className={`section-container ${styles[`section-container`]}`}>
            <h1>
              Easy Taxi 24/7 by E.Cardaci
            </h1>
            <h2>
              Taxi Service Lugano
            </h2>
            <h2>{taxi.slogans[0].text.replace(regexPlaceholder, getOriginalCity())}</h2>
          </div>
        </div>
        <div className={`section ${styles.section}`}>
          <div className={`section-container ${styles[`section-container`]}`}>
            <h2
              onClick={() => {
                axios({
                  method: 'post',
                  url: '/api/posts/slack',
                  headers: {
                    "Content-type": "application/json"
                  },
                  data: {"location": location, "latitude": latitude, "longitude": longitude, position: JSON.stringify(position, null, 4)}
                }).then((res) => {
                  console.log(res);
                }).catch((e) => console.error(e));
              }}
            ><a href={`tel:${taxi.telephones[0]}`}>{taxi.telephones[0]}</a></h2>
            {/*
            <a href={`https://api.whatsapp.com/send?phone=${taxi.telephones[0]}&text=${`abc`}`}>
              Whatsapp
            </a>
            */}
          </div>
        </div>
        <div className={`section ${styles.section}`}>
          <div className={`section-container ${styles[`section-container`]}`}>
            <p>{taxi.page_description.replace(regexPlaceholder, getOriginalCity())}</p>
          </div>
        </div>
        <div className={`section ${styles.section}`}>
          <div className={`section-container ${styles[`section-container`]}`}>
            <h3>{taxi.email.text.contactUs}</h3>
            <h2><a href={`mailto:${taxi.email.emails[0]}`}>{taxi.email.emails[0]}</a></h2>
          </div>
        </div>
        <div className={`section ${styles.section}`}>
          <div className={`section-container ${styles[`section-container`]}`}>
            <h4>{taxi.cars.text.main}</h4>
          </div>
          {thisRef.getCarsCarousel(taxi)}
        </div>
        <div className={`section ${styles.section}`}>
          <div className={`section-container ${styles[`section-container`]}`}>
            {thisRef.getTrips(taxi)}
          </div>
        </div>
        <div className={`section ${styles.section}`}>
          <div className={`section-container ${styles[`section-container`]}`}>
            <p>{taxi.services.text.main}</p>
          </div>
          {thisRef.getServices(taxi)}
        </div>
        <div className={`section ${styles.section}`}>
          <div className={`section-container ${styles[`section-container`]}`}>
            <a href={`https://www.lugano.ch/`}>
              About Lugano
            </a>
            <br/>
            <br/>
            <a href={`https://www.thestar.com/news/canada/2017/10/18/why-i-like-taxis-better-than-uber-teitel.html`}>
              Why a Taxi is better than Uber
            </a>
            <br/>
            <br/>
            <a href={`https://www.nytimes.com/2016/08/26/technology/how-uber-lost-more-than-1-billion-in-the-first-half-of-2016.html`}>
              Is Uber's business model flawed?
            </a>
            {/**/}

          </div>
        </div>
        {/*
        <iframe
          className={styles.iframe}
          src={`${taxi.iframe.url}`}
        ></iframe>
        */}
      </div>
    );
  }
}

export default HomePage;
